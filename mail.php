<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$servername = "localhost";
		$username = "wingsfou_admin";
		$password = "wings@fly&fou";
		$database = "wingsfou_wings";

		$conn = mysqli_connect($servername, $username, $password , $database);

		// if (!$conn) {
	 //    die("Connection failed: " . mysqli_connect_error());
		// }
		// echo "Connected successfully";


		function query($sql){
			global $conn;
			return mysqli_query($conn,$sql);
		}
		function confirm($result){
			global $conn;
			if(!$result) {
				http_response_code(403);
        echo "There was a problem with your submission, please try again.";
				die("QUERY FAILED " . mysqli_error($conn));
			}else{
				http_response_code(200);
	      echo "Thank You! Your message has been sent.";
			}
		}


		$name = strip_tags(trim($_POST["name"]));
		$name = str_replace(array("\r","\n"),array(" "," "),$name);
		$email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
    $message = trim($_POST["message"]);
		$date = date("d/m/Y");

		if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
	            http_response_code(400);
	            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
	            exit;
	  }
		$query = query("INSERT INTO email(date,name, email, message) VALUES('{$date}','{$name}', '{$email}','{$message}')");
		confirm($query);

		// $recipient = "shetty.raxx555@gmail.com";
	 //  $subject = "New contact from $name";
	 //  $email_content = "Name: $name\n";
	 //  $email_content .= "Email: $email\n\n";
	 //  $email_content .= "Message:\n$message\n";
	 //  $email_headers = "From: $name <$email>" . "Content-type: text";
	 //  if (mail($recipient, $subject, $email_content, $email_headers)) {
	 //            http_response_code(200);
	 //            echo "Thank You! Your message has been sent.";
	 //        } else {
	 //            http_response_code(500);
	 //            echo "Oops! Something went wrong and we couldn't send your message.";
	 //        }
		mysqli_close($conn);
   } else {
         http_response_code(403);
        echo "There was a problem with your submission, please try again.";
     }

?>
