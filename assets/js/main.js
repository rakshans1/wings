$(document).ready(function(){
	loader();
	aimBubbleClick();
	setInterval(function(){
		eventTada();
	},4000);
	$(window).load(function(){
		fb();
	});
});

(function ($){
	var $mobileNavToogleBtn = $('.mobile-nav-toggle');

	function onBtnClick (e){
		var $this = $(this),
				$selectors =  $('.mobile-nav');
		$this.toggleClass('is-open');
		$selectors.toggleClass('is-open');
	}
	$(document).ready(function(){
		$mobileNavToogleBtn.on('click' , onBtnClick);
	});
})(jQuery);

function mobileNav() {
  $('.mobile-nav-toggle').on('click', function(){
    var status = $(this).hasClass('is-open');
    if(status){ $('.mobile-nav-toggle, .mobile-nav').removeClass('is-open'); }
    else { $('.mobile-nav-toggle, .mobile-nav').addClass('is-open'); }
  });
}

function loader(){
        $('.home-sections').addClass('loaded');
}
function fb(){
(function(d, s, id) {
	 var js, fjs = d.getElementsByTagName(s)[0];
	 if (d.getElementById(id)) return;
	 js = d.createElement(s); js.id = id;
	 js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=1357058854311378";
	 fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
}
function eventTada(){
	var randNum = Math.floor(Math.random() * $('.article-thumb').length) + 1;
	$('.article-thumb').eq(randNum).addClass('is-emph')
		.siblings().removeClass('is-emph');
}


function aimBubbleClick(){
	$('.face').on('click',function()  {
		var $this = $(this),
					faceTop = $this.position().top,
					vertMath = -1 * (faceTop - 230);
					faceLeft = $this.position().left,
					horizMath = 0 - faceLeft;

		if($(window).width()>640){
			$this.parent().css('top', + vertMath + 'px');
		}else{
			if($this.hasClass('back-btn')){
				aimNarrowStart();
			}else{
				$this.parent().css('left', + horizMath + 'px');
			}
		}
		if(!$this.hasClass('back-btn')){
			$this.addClass('has-bubble-open').siblings().removeClass('has-bubble-open');
		}
	});

}

$(window).scroll(function()  {
	eventVidScroll();
	startAiming();
	startEvents();
	
});

function eventVidScroll()  {
	var wScroll = $(window).scrollTop();
	$('.video-strip').css('background-position','center -'+ wScroll +'px');
}

function startAiming() {
	var wScroll = $(window).scrollTop();
	if($('.aim').offset().top - $(window).height()/2 < wScroll ){
		if($(window).width()>640 ){
			$('.faces').addClass('launched');
			if(!$('.face').hasClass('has-bubble-open')){
				setTimeout(function() {
					$('.face:nth-child(3)').addClass('has-bubble-open');
				},400);
		  }
		}	else {
			aimNarrowStart();
		}
	}
}
function startEvents(){
	var wScroll = $(window).scrollTop();
	if($('.event').offset().top - $(window).height()/2 < wScroll ){
		$('.article-thumb').each(function(i){
			setTimeout(function(){
				$('.article-thumb').eq(i).addClass('is-visible');
			},200 * i);
		});
	}

}

function aimNarrowStart() {
	$('.faces').css({
		'top' : '230px',
		'left': '0px'
	});
	$('.face').first().addClass('has-bubble-open')
	.siblings().removeClass('has-bubble-open');
}
function aimWideStart() {
	$('.faces').css({
		'top' : '0px',
		'left': '0px'
	});
	$('.face:nth-child(3)').addClass('has-bubble-open')
	.siblings().removeClass('has-bubble-open');
}
$(window).resize(function() {
	if($(window).width()>640){
		aimWideStart();
	}else{
		aimNarrowStart();
	}
});



$(function(){
	var form = $('#contact-form');
	var formMessages = $('#form-messages');
	var form_signup_box = $('#form-signup-box');
	$(form).submit(function(e) {
		e.preventDefault();
		var formData = $(form).serialize();
		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: formData
		})
		.done(function(response){
			$(formMessages).removeClass('error');
    	$(formMessages).addClass('success');
    	$(form_signup_box).addClass('success');
    	$(formMessages).text(response);
    	$('#name').val('');
    	$('#email').val('');
    	$('#message').val('');
		})
		.fail(function(){
			$(formMessages).removeClass('success');
    	$(formMessages).addClass('error');
    	if(data.responseText !== '') {
    		$(formMessages).text(data.responseText);
    	} else {
    		$(formMessages).text('Oops! Message could not be sent.');
    	}
		});
	});
	
	
});

function photo_modal(req){
	debugger;
	var modal = document.getElementById('myModal');
	var img = document.getElementById("img01");
	var caption = document.getElementById("caption");
	modal.style.display = "block";
	img.src = req.src;
	img.alt = req.alt;
	caption.innerHTML = req.alt; 

}