<?php
		$servername = "localhost";
		$username = "wingsfou_admin";
		$password = "wings@fly&fou";
		$database = "wingsfou_wings";
		$conn = mysqli_connect($servername, $username, $password , $database);
    function query($sql){
			global $conn;
			return mysqli_query($conn,$sql);
		}
    function confirm($result){
      global $conn;
      if(!$result) {
        die("QUERY FAILED " . mysqli_error($conn));
	     }
     }
     function fetch_array($result){
       return mysqli_fetch_array($result);
     }
    function get_message(){
$query = query(" SELECT * FROM email ");
confirm($query);
while($row = fetch_array($query)) {
$date = $row['date'];
$name = $row['name'];
$email = $row['email'];
$message = $row['message'];
$product = <<<DELIMETER
  <tr>
		<td class="table-name" data-title="Date">$date</td>
    <td class="table-name" data-title="Name">$name</td>
    <td data-title="Email" > $email</td>
    <td data-title="Message">$message</td>
  </tr>
DELIMETER;
echo $product;
        }
}
?>
<!DOCTYPE HTML>
<html>
  <head >
    <meta name=robots content="nofollow">
    <title>Messages</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon" sizes="32x32">
    <style >
    .table {
      margin-bottom: 0px;
      font-size: 16px;
      font-weight: 300;
      width: 100%;
      max-width: 100%;
      }
      .table tr {
      border-bottom: 1px solid #ffa517;
      }
      .table td {
        padding: 12px 8px;
      }
      ::before, ::after {
      box-sizing: border-box;
      }
      @media (max-width: 600px) {
      .table {
        font-size: 14px;
      }
     thead tr {
		     position: absolute;
		       top: -9999px;
		         left: -9999px;
	          }
      .table table, .table thead, .table tbody, .table th, .table td, .table tr {
        display: block;
      }
      .table td {
        border: none;
        position: relative;
        padding-left: 50%;
        white-space: normal;
        text-align: left;
      }
      .table td:before {
        position: absolute;
        top: 6px;
        left: 6px;
        width: 45%;
        padding-right: 10px;
        white-space: nowrap;
        text-align: left;
        font-weight: 600;
        color: #666;
      }
      .table td:before {
        content: attr(data-title);
      }
      }
    </style>
  </head>
  <body>
    <div class="table_block">
      <table class="table">
        <thead>
					<td>Date</td>
          <td>Name</td>
          <td>Email</td>
          <td>Message</td>
        </thead>
        <tbody>
          <?php get_message();?>
        </tbody>
      </table>
    </div>
  </body>
</html>
